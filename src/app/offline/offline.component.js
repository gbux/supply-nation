angular.module('Offline',[]).component('offlineComponent', {
    templateUrl: './src/app/offline/offline.tpl.html',
    controller: OfflineCtrl,
      bindings: {
   
    }
  })
  OfflineCtrl.$inject = ['$rootScope','$scope']
  
  function OfflineCtrl($rootScope,$scope) {
   
   $scope.offlineStatus =  navigator.onLine ? "ONLINE" : "OFFLINE";
   
  }