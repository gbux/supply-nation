angular.module('Command').factory('CommandFactory',CommandFactory)



CommandFactory.$inject = ['$http','$q','$timeout','$location']
function CommandFactory($http,$q,$timeout,$location){
    return {
        getProducts : function(){
            return  $http.get('http://localhost:5050/myProducts')
        },
        getCommands : function(){
            return  $http.get('http://localhost:5050/commandes')
        },
        getCommand : function(commande){
            return  $http.get('http://localhost:5050/commandes/'+commande)
        },
        addCommand :function(data){
            console.log("Datas to add",data)
            return $http.post('http://localhost:5050/commandes/',data);
        }
    }
}