angular.module('Command', []).component('commandComponent', {
    templateUrl: './src/app/command/command.tpl.html',
    controller: CommandCtrl,
    bindings: {

    }
})
CommandCtrl.$inject = ['$rootScope', '$scope', 'CommandFactory']

function CommandCtrl($rootScope, $scope, CommandFactory) {
    $scope = this;

    $scope.newCommand = {
        title: '',
        details: {},
        offline:''
    }

    $scope.products;



/*    
$scope.commands = localStorage.getItem('commandes'); 

CommandFactory.getCommands().then(function (response) {
        $scope.commands = response.data
        $scope.products = $scope.commands[0].products
    })*/

    CommandFactory.getProducts().then(function (response) {
        $scope.productsInitial = response.data
        $scope.newCommand.details = $scope.productsInitial;
        for(i in $scope.productsInitial){
            $scope.newCommand.details[i].quantity = 0;
        }

    })
    $scope.getCommands = function () {
        $scope.commands = JSON.parse(localStorage.getItem('commandes')) || [];
        console.log($scope.commands)
    }
    $scope.addCommandSubmit = function () {
        $scope.commands = JSON.parse(localStorage.getItem('commandes')) || [];
        $scope.commands.push($scope.newCommand)
        localStorage.setItem('commandes', JSON.stringify($scope.commands)); 
    }
    $scope.getCommands()

}