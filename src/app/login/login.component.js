angular.module('Login', []).component('loginComponent', {
    templateUrl: './src/app/login/login.tpl.html',
    controller: LoginCtrl,
    bindings: {

    }
})
LoginCtrl.$inject = ['$rootScope', '$scope']

function LoginCtrl($rootScope, $scope) {
    console.log("2",$rootScope)
    $scope.user = {
        "email": ''
    };
    $scope.login = function(){
        
        var regMail = new RegExp(/^[a-zA-Z0-9_.+-]+@supplynation\.com$/g);
        if (!regMail.test($scope.user.email)) {
            console.log("error email!!")
            $scope.error = true;
        }else{
            console.log("good email!!")
            $rootScope.currentPage = 'command';
        }

    }
}