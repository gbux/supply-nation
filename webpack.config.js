const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry:[
        './src/main.js',
        'font-awesome/scss/font-awesome.scss'
    ],
    output: {
        path:__dirname+'/dist',
        filename:'bundle.[hash].js'
    },
    plugins:[
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']
        })
    ],
    module: {
        rules: [
            {
                test: /\.html$/,
                use: 'raw-loader'
              },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", 
                    "css-loader", 
                    "sass-loader" // compiles Sass to CSS


                ]
            },
            {
                test: /\.css$/,
                use:
                    [
                        'style-loader',
                        'css-loader'
                    ]
            }
            ,
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, // to support @font-face rule
                loader:
                    'url-loader?limit=4000000'
            }
            ,
            {
                test: /\.ico|png|jpg$/, // Favicon
                loader:
                    'file-loader',
                options:
                    {
                        name: 'css/[name].[ext]'
                    }
            }
          
        ]
    }
}